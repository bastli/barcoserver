#!/bin/bash

make CC=${CC:-arm-linux-musleabihf-gcc} "CFLAGS=-I../libuv/include/ -O5 -L../libuv/.libs/ -static" "$@"
