#pragma once
#include <uv.h>


typedef struct{
	size_t length;
	char *msg;
	
	/*
	 * The message source. The pointer has to guarantee to be 
	 * dereferencable to at least type uv_handle_t.
	 */
	void *source;
}Message;


int Message_answer(const Message *msg, const char *str, size_t len);
int Message_answer_dyn(const Message *msg, const char *str);

#include <util.h>
#define Message_answer_static(MSG, STR) Message_answer(MSG, STR, LENGTH(STR));
