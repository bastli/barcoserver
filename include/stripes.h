#pragma once
#include <stdint.h>

#define STRIPE_LED_CNT 112
#define STRIPE_CNT 15
#define BYTES_PER_STRIPE STRIPE_LED_CNT*3
#define STRIPE_OFFSET 512

typedef struct{
	uint8_t r,g,b;
}Color;

/**
 * The patch table
 * Usually you do not want to mess with this yourself
 */
extern uint8_t Stripe_patch_tbl[STRIPE_CNT];

/**
 * Resolve a physical port to a logical port
 * 
 * @bug This function does not return special values on error,
 * but rather the valid 0.
 * 
 */
uint8_t Stripe_patch_resolve(uint8_t physical);

/**
 * Patch the logical port to the physical one.
 * 
 * If the physical port is already patched by another logical port,
 * it will patch it anyways.
 */
uint8_t Stripe_patch(uint8_t logical, uint8_t physical);

/**
 * Reset the patching, so every logical port points to the same physical one
 */
void Stripe_patch_reset(void);

/**
 * Load a patching scheme from a file.
 * 
 * The (plain-text) filelayout is as follows:
 * Every line describes one logical port in ascending order, so line 0
 * id for logical port 0, ...
 * The line contains only the physical port number in an ascii-string, 
 * to which the logical port is mapped to, and is terminated with _only_ '\n'
 */
int Stripe_patch_load(const char *filename);

/**
 * Save the patching table to a file.
 * 
 */
int Stripe_patch_save(const char *filename);

/**
 * Initialize the stripes-module.
 * 
 * This opens the character device.
 */
int Stripes_init(void);

/**
 * Seek the character file to the specified (physical) stripe and led offset.
 * 
 * If you want to write to a whole stripe, set led=0;
 */
int Stripe_seek_p(uint8_t stripe_p, int led);

/**
 * Seek to the specified (logical) stripe and led offset
 */
int Stripe_seek(uint8_t stripe, int led);

/**
 * Write a whole led set to the character device.
 * 
 * No dimension check will and can be done, so make sure that col
 * really holds BYTES_PER_STRIPE bytes
 */
int Stripe_write_set(uint8_t col[BYTES_PER_STRIPE]);

/**
 * Set the led RGB-Values of a single (logical) stripe
 */
int Stripe_set(uint8_t stripe, uint8_t col[BYTES_PER_STRIPE]);

/**
 * Ditto, but with physical stripes
 */
int Stripe_set_p(uint8_t stripe, uint8_t col[BYTES_PER_STRIPE]);

/**
 * Write a single color to the stripe device
 */
int Stripe_write_color(Color color);

/**
 * Write a single color to a specific LED on a stripe
 */
int Stripe_set_led(uint8_t stripe, int led, Color color);

/**
 * Ditto, but with pyhsical stripes
 */
int Stripe_set_led_p(uint8_t stripe, int led, Color color);

/**
 * Deinitialize the stripes-module.
 * 
 * This closes the character device
 */
int Stripes_deinit(void);

/**
 * Display a number on the specified (physical!) stripe.
 * 
 * A number is represented as dashes. To read the number, 
 * just count the number of dashes.
 */
void Stripe_display_number(uint8_t stripe, uint8_t num);
