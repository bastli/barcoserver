#pragma once
#include <stdint.h>
#include <server.h>
#include <util.h> //LENGTH

typedef int(*Command_cb)(Message *msg);

typedef struct{
	const char* name;
	const char* description;
	const char* usage;
	const char* opcode;
	
	Command_cb callback;
} ASCII_Command;

typedef struct{
	const char* name;
	const char* description;
	uint32_t opcode;
	uint16_t payload_length;
	
	Command_cb callback;
} Command;

typedef struct{
	size_t length;
	Command* commands;
} Command_Set;

typedef struct{
	size_t length;
	ASCII_Command* commands;
} ASCII_Command_Set;

/**
 * Search for an opcode in the command-set
 * 
 * @return Pointer to the command or NULL if opcode not found
 */
Command* Command_search(const Command_Set *cs, uint32_t opcode);

/**
 * Search for an opcode in the command-set
 * 
 * @return Pointer to the command or NULL if opcode not found
 */
ASCII_Command* ASCII_Command_search(const ASCII_Command_Set *cs, Message *msg);

/**
 * Execute a command extracted from a message
 * 
 * This will automgically execute the callback associated with the command.
 */
int Command_execute(const Command_Set *cs, Message *msg);

/**
 * Execute a command extracted from a message
 * 
 * This will automgically execute the callback associated with the command.
 */
int ASCII_Command_execute(const ASCII_Command_Set *cs, Message *msg);

/**
 * Responds to the message msg with a usage reference with all commands in cs
 */
int ASCII_Commandset_help(const ASCII_Command_Set *cs, Message *msg);

/**
 * Format a command set out of a (STATIC!) command array
 */
#define COMMANDSET(CMDS) {.length=LENGTH(CMDS), .commands=CMDS}
