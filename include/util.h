#pragma once
#include <stdlib.h>
#include <message.h>

enum LOG_LEVEL{
	DEBUG,
	INFO,
	WARN,
	ERROR,
	FATAL
};

extern enum LOG_LEVEL loglevel;

/**
 * The main logging function.
 * 
 * Acts like a call to printf, with an additional log_level argument at the beginning
 */
#define do_log(LEVEL,MSG,...) _do_log(LEVEL,__FILE__, __LINE__, __func__, MSG, ##__VA_ARGS__)

/**
 * Internal function for logging
 */
void _do_log(enum LOG_LEVEL level, const char *file, size_t line, const char* function, const char* fmt, ...);

/**
 * Does haystack begin with needle?
 * 
 * If yes, return true, false otherwise
 */
int strbegins(const char *haystack, const char*needle);

/**
 * Does the message begin with needle?
 * 
 * If yes, return true, false otherwise
 */
int msgbegins(const Message *haystack, const char*needle);

/**
 * Get the length of a STATIC(!) array
 */
#define LENGTH(ARR) sizeof(ARR)/sizeof(*ARR)
