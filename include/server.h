#pragma once
#include <uv.h>
#include <message.h>

#define LISTEN_BACKLOG 10

enum SERVER_TYPE{
	SERVER_TYPE_UDP,
	SERVER_TYPE_TCP,
};


typedef int(*Server_msg_cb)(Message *msg);

typedef struct{
	enum SERVER_TYPE type;
	const char* addr;
	unsigned short port;
	
	Server_msg_cb callback;
} Server;


extern uv_loop_t Server_Loop;

/**
 * Initialize the global server instance.
 * 
 * This sets up the eventloop and signal handlers
 */
int Server_init(void);

/**
 * Add a new server instance
 * 
 * Once this function has been called,
 * Server_run will serve this address
 * 
 */
int Server_add(Server *serv);

/**
 * Enter the main event-loop
 */
int Server_run(void);

/**
 * Request a server stop.
 * 
 * This doesn't stop the server immediately, but rather ends Server_run()
 * at the next convenient moment.
 */
int Server_stop(void);

/**
 * Close all connections and free all memory
 * 
 */
int Server_destroy(void);
