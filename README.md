This software makes the barco strips accessible through comfortable IP Services.

Dependencies
============

* libuv

Zybo Board
==========

The board is an ARM A9 core running some distribution (which? custom?), with a busybox-shell, sshd and sysvinit.
The disk is mounted read-only (because powercycling may and will occur), and it will assign itself a static ip address `10.6.66.10`

Toolchain
------------
You can use the xilinx-toolchain, but a free variant works too. If you use Ubuntu(or maybe Debian) `apt-get install gcc-4.8-arm-linux-gnueabihf` might work.
Otherwise, the tuple "arm-linux-gnueabihf" works fine.

BarcoServer
---------------

The BarcoServer is startet in the init process, see `/etc/init.d/S60barcoapp` (Or something similar, please check)
The actual binary, that this script executes, is `/home/root/BarcoServer`
### Updating the binary ###
If you want to upload a new version of the Binary onto the zybo-board, follow these steps:

1. Log in via ssh: `ssh root@10.6.66.10`. The password is rXQgkEHDeZzy
2. Remount / read-write: `mount -o remount,rw /`
3. Copy the Binary. This step is individual, but something like `scp BarcoServer root@10.6.66.10:/home/root/BarcoServer` will do.
4. Flush buffers: `sync`
5. Remount / read-only: `mount -o remount,ro /`

API's
=====

UDP Port 1337
-------------------
The UDP Port 1337 allows to push a buffer to a single stripe.
A datagram consists of  one byte stripe address, and 112*3 Bytes RGB-Values.

TCP Port 1338
-------------------
The TCP Port 1338 implements binary control mechanisms.
A control message consists of 4 bytes opcode(Note: Big endian coding, use a function like htons) and a variable length payload.

There are various opcodes:
* 0x0000000 + 3 Bytes RGB: Set the color of all stripes to an RGB-Value
* 0x0000001 + 0 Bytes: Stop the process.
* 0x0000002 + 1 Byte Index + 3 Bytes RGB: Set the color of the whole string to RGB
* 0xfefefefe + 0 Bytes: Display a numbering according to the physical port number
* 0xefefefef + 0 Bytes: Display a numbering according to the logical port number
* 0xb16b00b5 + 1 Byte index + 3*112 Bytes RGB: Set the colors of a single (logical) stripe to the bytes specified
* 0xcafebabe + 1 Byte index + 3*112 Bytes RGB: Set the colors of a single (physical) stripe to the bytes specified
* 0xdeadbeef + 1 Byte physical port + 1 Byte logical port: Patch the specified logical port to the physical port given.

TCP Port 1339
-------------------
This port implements ASCII-Control to control certain features in a simple command line.
The commands must be '\n'-terminated (telnet usually terminates with "\r\n"). Spaces, it seems, aren't important (if you want to know why, see sscanf(3) and tell Dominik)

* `patch <logical> -> <physical>`: Patch the <logical> port to the <physical> port.
* `list-patch`: Returns all entries of the patching table, in the format `<logical> -> <phsyical>`

If everything works out, the server will respond with "OK\n", otherwise some error will be printed.
