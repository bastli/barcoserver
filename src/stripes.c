#include <stripes.h>
#include <util.h>
#include <errno.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>

int Stripe_fd;

uint8_t Stripe_patch_tbl[STRIPE_CNT];

uint8_t Stripe_patch_resolve(uint8_t logical){
	if(logical < STRIPE_CNT){
		return Stripe_patch_tbl[logical];
	}
	return 0;
}

uint8_t Stripe_patch(uint8_t logical, uint8_t physical){
	if(logical >= STRIPE_CNT || physical >= STRIPE_CNT){
		do_log(WARN, "Invalid patch %hhu -> %hhu specified", logical, physical);
	}
	do_log(INFO, "Patching %hhu -> %hhu", logical, physical);
	Stripe_patch_tbl[logical] = physical;
	return 0;
}

void Stripe_patch_reset(void){
	do_log(DEBUG, "Reset stripe patching");
	for(unsigned int i=0; i<STRIPE_CNT; i++){
		Stripe_patch_tbl[i]=i;
	}
}

int Stripe_patch_load(const char *filename){
	FILE *f=fopen(filename, "r");
	if(f==NULL){
		do_log(ERROR, "Could not open file %s", filename);
		return -1;
	}
	for(unsigned int i=0; i<STRIPE_CNT; i++){
		fscanf(f, "%hhu\n", &Stripe_patch_tbl[i]);
	}
	fclose(f);
	return 0;
}

int Stripe_patch_save(const char *filename){
	FILE *f=fopen(filename, "w+");
	if(f==NULL){
		do_log(ERROR, "Could not open file %s", filename);
		return -1;
	}
	for(unsigned int i=0; i<STRIPE_CNT; i++){
		fprintf(f, "%hhu\n", Stripe_patch_tbl[i]);
	}
	fclose(f);
	return 0;
}

int Stripes_init(void){
	Stripe_fd=open("/dev/barco0",O_RDWR);
	if(Stripe_fd < 0){
		do_log(FATAL,"Couldn't open barco device. %s", strerror(errno));
		return -1;
	}
	Stripe_patch_reset();
	do_log(INFO, "Barco-device successfully added");
	return 0;
}

int Stripe_seek_p(uint8_t stripe_p, int led){
	if(stripe_p >= STRIPE_CNT){
		do_log(WARN, "Stripe index %hhu out of range. Ignoring", stripe_p);
		return -1;
	}
	if(led >= STRIPE_LED_CNT){
		do_log(WARN, "LED index %d out of range. Ignoring", led);
		return -1;
	}
	
	do_log(DEBUG, "Seeking to stripe %hhu, led %d", stripe_p, led);
	off_t off=(stripe_p+1)*STRIPE_OFFSET + led*3;
	off_t new_off=lseek(Stripe_fd, off, SEEK_SET);
	if(off != new_off){
		do_log(ERROR, "Couldn't seek to offset %d. %s", off, strerror(errno));
		return -1;
	}
	return 0;
}

int Stripe_seek(uint8_t stripe, int led){
	if(stripe >= STRIPE_CNT){
		do_log(WARN, "Stripe index %hhu out of range. Ignoring", stripe);
		return -1;
	}
	if(led >= STRIPE_LED_CNT){
		do_log(WARN, "LED index %d out of range. Ignoring", led);
		return -1;
	}
	return Stripe_seek_p(Stripe_patch_resolve(stripe), led);
}


//Assert col length = STRIPE_LED_CNT*3
int Stripe_write_set(uint8_t col[BYTES_PER_STRIPE]){
	ssize_t ret=write(Stripe_fd, col, BYTES_PER_STRIPE);
	if(ret != STRIPE_LED_CNT*3){
		do_log(ERROR, "Write to barco-device failed.");
		return -1;
	}
	
	return 0;
}
int Stripe_set(uint8_t stripe, uint8_t col[BYTES_PER_STRIPE]){
	if(Stripe_seek(stripe, 0)<0){
		return -1;
	}
	return Stripe_write_set(col);
}
int Stripe_set_p(uint8_t stripe, uint8_t col[BYTES_PER_STRIPE]){
	if(Stripe_seek_p(stripe, 0)<0){
		return -1;
	}
	return Stripe_write_set(col);
}

int Stripe_write_color(Color color){
	ssize_t nwrite=0;
	nwrite+=write(Stripe_fd, &color.r, 1);
	nwrite+=write(Stripe_fd, &color.g, 1);
	nwrite+=write(Stripe_fd, &color.b, 1);
	if(nwrite != 3){
		do_log(ERROR, "Error writing to barco-device");
		return -1;
	}
	return 0;
}
int Stripe_set_led(uint8_t stripe, int led, Color color){
	if(Stripe_seek(stripe, led)<0){
		return -1;
	}
	return Stripe_write_color(color);
}

int Stripe_set_led_p(uint8_t stripe, int led, Color color){
	if(Stripe_seek_p(stripe, led)<0){
		return -1;
	}
	return Stripe_write_color(color);
}

void Stripe_display_number(uint8_t stripe, uint8_t num){
	Color on={0xFF,0,0},off={0,0,0};
	for(unsigned int i=0; i<STRIPE_LED_CNT; i++){
		if(i%7>3 && i/7<num){
			Stripe_set_led_p(stripe,i,on);
		}
		else{
			Stripe_set_led_p(stripe,i,off);
		}
	}
}

int Stripes_deinit(void){
	close(Stripe_fd);
	return 0;
}


