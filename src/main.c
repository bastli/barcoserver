#include <server.h>
#include <command.h>
#include <util.h>
#include <unistd.h>
#include <signal.h>
#include <stripes.h>
#include <stdio.h>
#include <uv.h>
#include <message.h>

int API_setstripe(Message *m){
	uint8_t *vals=(uint8_t *)m->msg;
	Stripe_set(*vals, vals+1);
	return 0;
}
int API_setstripe_p(Message *m){
	uint8_t *vals=(uint8_t *)m->msg;
	Stripe_set_p(*vals, vals+1);
	return 0;
}
int API_setstripe_single(Message *m){
	uint8_t *vals=(uint8_t *)m->msg;
	Color c;
	c.r=vals[1];
	c.g=vals[2];
	c.b=vals[3];
	for(int i=0; i<STRIPE_LED_CNT; i++){
		Stripe_set_led(*vals, i, c);
	}
	return 0;
}
int API_patch(Message *m){
	uint8_t *vals=(uint8_t *)m->msg;
	Stripe_patch(vals[1],vals[0]);
	return 0;
}
int API_stop(Message *m){
	Server_stop();
	return 0;
}

int API_setstripes(Message *m){
	uint8_t* vals=(uint8_t*)m->msg;
	Color c={vals[0],vals[1],vals[2]};
	for(unsigned int i=0; i<STRIPE_CNT; i++){
		for(unsigned int a=0; a<STRIPE_LED_CNT; a++){
			Stripe_set_led(i,a,c);
		}
	}
	return 0;
}

int API_display_port_p(Message *m){
	for(unsigned int i=0; i<STRIPE_CNT; i++){
		Stripe_display_number((uint8_t)i, i);
	}
	return 0;
}
int API_display_port(Message *m){
	for(unsigned int i=0; i<STRIPE_CNT; i++){
		Stripe_display_number(Stripe_patch_resolve((uint8_t)i), i);
	}
	return 0;
}
int API_ascii_patch(Message *msg){
	uint8_t p,l;
	int ret=sscanf(msg->msg, "patch %hhu -> %hhu\n", &l, &p);
	if(ret!=2){
		do_log(ERROR, "Wrong Syntax");
		Message_answer_static(msg, "Wrong Syntax.\npatch <logical> -> <physical>\n");
		return -1;
	}
	ret=Stripe_patch(l,p);
	if(ret < 0){
		Message_answer_static(msg, "Patching failed.\n");
		return -1;
	}
	return 0;
}
int API_ascii_list_patch(Message *msg){
	Message_answer_static(msg, "Logical -> Physical\n");
	char buf[64];
	for(uint8_t i=0; i<STRIPE_CNT; i++){
		size_t nwrite=snprintf(buf,sizeof(buf), "%hhu -> %hhu\n", i, Stripe_patch_resolve(i));
		Message_answer(msg, buf, nwrite);
	}
	return 0;
}
const ASCII_Command ASCII_API[]={
	{
		.name="Patch",
		.usage="patch i -> j",
		.description="Patch the i-th logical to the j-th physical port",
		.opcode="patch",
		.callback=&API_ascii_patch
	},
	{
		.name="ListPatch",
		.usage="list-patch",
		.description="Write the logical port",
		.opcode="list-patch",
		.callback=&API_ascii_list_patch
	}
};
const Command TCP_API[]={
	{
		.name="SetStripes",
		.description="Set the color of all stripes",
		.opcode=0,
		.payload_length=3, //RGB
		.callback=&API_setstripes
	},
	{
		.name="Stop",
		.description="Stop the process",
		.opcode=1,
		.payload_length=0,
		.callback=&API_stop
	},
	{
		.name="SetStripeSingle",
		.description="Set the color of a single (logical) stripe to a single color",
		.opcode=2,
		.payload_length=1+3, //index + RGB
		.callback=&API_setstripe_single
	},
	{
		.name="SetStripe",
		.description="Set the color of a single (logical) stripe",
		.opcode=0xb16b00b5,
		.payload_length=1+BYTES_PER_STRIPE, //index + Color data
		.callback=&API_setstripe
	},
	{
		.name="Physical_Numbers",
		.description="Show the Physical port number",
		.opcode=0xfefefefe,
		.payload_length=0,
		.callback=&API_display_port_p
	},
	{
		.name="Logical_Numbers",
		.description="Show the logical port number",
		.opcode=0xefefefef,
		.payload_length=0,
		.callback=&API_display_port
	},
	{
		.name="SetPhysicalStripe",
		.description="Set the color of a single (physical) stripe",
		.opcode=0xcafebabe,
		.payload_length=1+BYTES_PER_STRIPE, //index + Color data
		.callback=&API_setstripe_p
	},
	{
		.name="Patch",
		.description="patch the second argument to the first physical argument",
		.opcode=0xdeadbeef,
		.payload_length=2, //0:physical <- 1:logical
		.callback=&API_patch
	},
};

Command_Set cs=COMMANDSET(TCP_API);
ASCII_Command_Set acs=COMMANDSET(ASCII_API);

	
int ascii_callback(Message *m){
	if(msgbegins(m, "help")){
		ASCII_Commandset_help(&acs, m);
	}
	else{
		ASCII_Command_execute(&acs, m);
	}
	return 0;
}
int tcp_callback(Message *m){
	Command_execute(&cs,m);
	return 0;
}

int udp_callback(Message *m){
	if(0&&m->length!=BYTES_PER_STRIPE){
		do_log(WARN, "Too few bytes in packet. Dropping");
		return -1;
	}
	Stripe_set(*(uint8_t*)(m->msg), ((uint8_t*)m->msg)+1);
	return 0;
}


void Reset_Strips(){
	uint8_t c[BYTES_PER_STRIPE] = {0};
	for(int i=0; i<STRIPE_CNT; i++){
		Stripe_set(i, c);
	}
}

int main(int argc, char**argv){
	loglevel=INFO;
	Server s={.type=SERVER_TYPE_TCP, .addr="0.0.0.0", .port=1338, .callback=&tcp_callback};
	Server ss={.type=SERVER_TYPE_UDP, .addr="0.0.0.0", .port=1337, .callback=&udp_callback};
	Server sss={.type=SERVER_TYPE_TCP, .addr="0.0.0.0", .port=1339, .callback=&ascii_callback};
	if(Stripes_init()<0){
		return 1;
	}
	Reset_Strips();
	if(Server_init()<0){
		Stripes_deinit();
	}
	char *savefile="/home/root/Barco_Patch_list.txt";
	if(access(savefile, R_OK|W_OK)==0){
		Stripe_patch_load(savefile);
	}
	Server_add(&s);
	Server_add(&ss);
	Server_add(&sss);
	
	Server_run();
	Server_destroy();
	Stripes_deinit();
	return 0;
}
