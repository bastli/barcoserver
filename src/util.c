#include <util.h>
#include <message.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>

enum LOG_LEVEL loglevel=DEBUG;

void _do_log(enum LOG_LEVEL level, const char *file, size_t line, const char* function, const char* fmt, ...){
	if(level>=loglevel){
		printf("[BarcoServer, %s@%s:%lu] ",file, function, line);
		va_list args;
		va_start(args, fmt);
		vprintf(fmt, args);
		va_end(args);
		putc('\n',stdout);
	}
}

int strbegins(const char *ptr1, const char *ptr2){
	while(1){
		if(*ptr1=='\0' || *ptr2=='\0'){
			break;
		}
		if(*ptr1 != *ptr2){
			return 1==0;
		}
		ptr1++;
		ptr2++;
	}
	return 1==1;
}

int msgbegins(const Message *msg, const char *ptr2){
	unsigned int i;
	const char *ptr1=msg->msg;
	for(i=0; i<msg->length; i++){
		if(*ptr1=='\0' || *ptr2=='\0'){
			break;
		}
		if(*ptr1 != *ptr2){
			return 1==0;
		}
		ptr1++;
		ptr2++;
	}
	return 1==1;
}
