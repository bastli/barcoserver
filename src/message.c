#include <message.h>
#include <string.h>


int Message_answer(const Message *msg, const char *str, size_t len){
	uv_stream_t *stream=(uv_stream_t*)msg->source;
	const uv_buf_t buf={.base=str, .len=len};
	return uv_try_write(stream, &buf, 1);
}
int Message_answer_dyn(const Message *msg, const char *str){
	return Message_answer(msg, str, strlen(str));
}
