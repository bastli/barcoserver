#include <server.h>
#include <netinet/in.h> //struct sockaddr_in
#include <util.h>
#include <stdlib.h>
#include <uv.h>
#include <stripes.h>

char Server_buf[BYTES_PER_STRIPE*STRIPE_CNT];

static void allocate_buffer(uv_handle_t* handle, size_t suggested_size, uv_buf_t* buf) {
	do_log(DEBUG, "Alloc");
	buf->base = Server_buf;
	buf->len = sizeof(Server_buf);
}

static void Server_free_handle(uv_handle_t *h){
	//Is this legal? Seems a bit fishy...
	//This is to free the memory allocated by the new uv_tcp/udp_t
	do_log(DEBUG, "Freeing handle");
	free(h);
}

static void Server_tcp_read(uv_stream_t *stream, ssize_t nread, const uv_buf_t *buf){
	do_log(DEBUG, "Received message with length %l", nread);
	if(nread == UV_EOF){
		do_log(INFO, "Client exited, removing...");
		uv_close((uv_handle_t*)stream, &Server_free_handle);
	}
	else if(nread<0){
		do_log(ERROR, "Error while reading from stream ");
	}
	else{
		Message msg;
		msg.length=nread;
		msg.source=(void*)stream;
		msg.msg=buf->base;
		Server_msg_cb callback=((Server*)((uv_handle_t*)stream)->data)->callback;
		if(callback!=NULL){
			callback(&msg);
		}
	}
}

static void Server_tcp_new_client(uv_stream_t *server, int status){
	if(status < 0){
		do_log(ERROR, "Error receiving new client. %s", uv_strerror(status));
		return;
	}
	do_log(INFO, "Received new client");

	uv_tcp_t *client = malloc(sizeof(uv_tcp_t));
	if(client == NULL){
		do_log(FATAL, "Allocation failure");
		return;
	}
	((uv_handle_t*)client)->data = ((uv_handle_t*)server)->data;
	
	int ret;
	ret=uv_tcp_init(&Server_Loop, client);
	if(ret<0){
		do_log(ERROR, "Couldn't initialize tcp stream. %s", uv_strerror(ret));
	}
	
	
	ret=uv_accept(server, (uv_stream_t*) client);
	if(ret < 0){
		do_log(ERROR, "Error accepting new client. %s", uv_strerror(ret));
		uv_close((uv_handle_t*)client, &Server_free_handle);
		return;
	}
    ret=uv_read_start((uv_stream_t*)client, &allocate_buffer, &Server_tcp_read);
	if(ret < 0){
		do_log(ERROR, "Can't read from client. %s", uv_strerror(ret));
		uv_close((uv_handle_t*)client, &Server_free_handle);
		return;
	}
	do_log(DEBUG, "Client succesfully added");
}

const char* Server_type_name(enum SERVER_TYPE st){
	switch(st){
		case SERVER_TYPE_TCP:
			return "TCP";
		case SERVER_TYPE_UDP:
			return "UDP";
		default:
			return "UNKNOWN";
	}
}

uv_loop_t Server_Loop;
uv_signal_t Server_signal;

static void Server_signal_callback(uv_signal_t* handle, int signum){
	Server_stop();
}

int Server_init(void){
	do_log(DEBUG, "Initializing UV-Loop");
	int ret=uv_loop_init(&Server_Loop);
	if(ret < 0){
		do_log(FATAL, "Couldn't initialize Eventloop. %s", uv_strerror(ret));
		return -1;
	}
	
	uv_signal_init(&Server_Loop, &Server_signal);
	uv_signal_start(&Server_signal, &Server_signal_callback, SIGINT);
	
	return 0;
}

void Server_udp_read(uv_udp_t *handle, ssize_t nread, const uv_buf_t *buf, const struct sockaddr *addr, unsigned flags){
	if(nread<0){
		do_log(FATAL, "Error while reading from stream ");
		Server_stop();
		return;
	}
	Message m;
	m.msg=buf->base;
	m.source=(void*)handle;
	m.length=nread;
	Server *serv=((uv_handle_t*)handle)->data;
	serv->callback(&m);
}

int Server_add_udp(Server *serv){
	uv_udp_t *server=malloc(sizeof(uv_udp_t));
	if(server==NULL){
		do_log(FATAL, "Allocation Error");
		return -1;
	}
	
	int ret;
	ret=uv_udp_init(&Server_Loop, server);
	if(ret < 0){
		do_log(ERROR, "Error initializing udp socket: %s", uv_strerror(ret));
		return -1;
	}
	
	((uv_handle_t*)server)->data=serv;
	
	struct sockaddr_in addr;
	uv_ip4_addr(serv->addr, serv->port, &addr);
	ret=uv_udp_bind(server, (const struct sockaddr*)&addr, UV_UDP_REUSEADDR);
	if(ret < 0){
		do_log(ERROR, "Error binding udp socket to address: %s", uv_strerror(ret));
		uv_close((uv_handle_t*)server, &Server_free_handle);
		return -1;
	}
	
	ret=uv_udp_recv_start(server, &allocate_buffer, &Server_udp_read);
	if(ret < 0){
		do_log(ERROR, "Could'nt start receiving on udp socket: %s", uv_strerror(ret));
		uv_close((uv_handle_t*)server, &Server_free_handle);
		return -1;
	}
	return 0;
}
int Server_add_tcp(Server *serv){
	uv_tcp_t *server=malloc(sizeof(uv_tcp_t));
	if(server==NULL){
		do_log(FATAL, "Allocation Error");
		return -1;
	}
	
	int ret;
	ret=uv_tcp_init(&Server_Loop, server);
	if(ret < 0){
		do_log(ERROR, "Error initializing tcp socket: %s", uv_strerror(ret));
		return -1;
	}
	((uv_handle_t*)server)->data=serv;
	
	struct sockaddr_in addr;
	uv_ip4_addr(serv->addr, serv->port, &addr);
	
	ret=uv_tcp_bind(server, (const struct sockaddr*)&addr, 0);
	if(ret < 0){
		do_log(ERROR, "Couldn't bind to address. %s", uv_strerror(ret));
		free(server);
		return -1;
	}
	
	ret=uv_listen((uv_stream_t*) server, LISTEN_BACKLOG, &Server_tcp_new_client);
	if(ret < 0){
		do_log(ERROR,"Error listening on socket. %s", uv_strerror(ret));
		free(server);
		return -1;
	}
	return 0;
}

int Server_add(Server *serv){
	do_log(INFO, "Adding new %s-server on %s:%hu", Server_type_name(serv->type), serv->addr, serv->port);
	if(serv->type==SERVER_TYPE_TCP){
		return Server_add_tcp(serv);
	}
	else{
		return Server_add_udp(serv);
	}
}

int Server_run(void){
	return uv_run(&Server_Loop, UV_RUN_DEFAULT);
}

int Server_stop(void){
	uv_stop(&Server_Loop);
	return 0;
}

int Server_destroy(void){
	uv_signal_stop(&Server_signal);
	int ret=uv_loop_close(&Server_Loop);
	if(ret<0){
		do_log(ERROR, "Couldn't close eventloop. %s", uv_strerror(ret));
		return -1;
	}
	return 0;
}
