#include <command.h>
#include <server.h>
#include <util.h>


Command* Command_search(const Command_Set *cs, uint32_t opcode){
	for(unsigned int i=0; i<cs->length; i++){
		if(cs->commands[i].opcode==opcode){
			return &(cs->commands[i]);
		}
	}
	return NULL;
}
int Command_execute(const Command_Set *cs, Message *msg){
	if(msg->length<4){ //opcode doesn't fit --> exit
		do_log(INFO, "Message too short for opcode, dropping");
		return -1;
	}
	uint32_t opcode;
	opcode=*(uint32_t*)(msg->msg); //Ugly C idiom to get the opcode
	opcode=ntohl(opcode); //Byte-order-correction
	
	const Command* cmd=Command_search(cs, opcode);
	if(cmd == NULL){
		do_log(INFO, "Opcode %x not found", opcode);
		return -1;
	}
	do_log(INFO, "Executing callback for ", cmd->name);
	Message new_msg;
	new_msg.length=msg->length-4;
	new_msg.source=msg->source;
	new_msg.msg=msg->msg+4;
	int ret=cmd->callback(&new_msg);
	return ret;
}


ASCII_Command* ASCII_Command_search(const ASCII_Command_Set *cs, Message *msg){
	unsigned int i;
	for(i=0; i<cs->length; i++){
		if(msgbegins(msg, cs->commands[i].opcode)){
			return &cs->commands[i];
		}
	}
	return NULL;
}

int ASCII_Commandset_help(const ASCII_Command_Set *cs, Message *msg){
	unsigned int i;
	for(i=0; i<cs->length; i++){
		ASCII_Command cmd=cs->commands[i];
		Message_answer_dyn(msg, cmd.name);
		Message_answer_static(msg, "\n\t");
		Message_answer_dyn(msg, cmd.usage);
		Message_answer_static(msg, "\n\t");
		Message_answer_dyn(msg, cmd.description);
		Message_answer_static(msg, "\n");
	}
	return 0;
}

int ASCII_Command_execute(const ASCII_Command_Set *cs, Message *msg){
	ASCII_Command *cmd=ASCII_Command_search(cs, msg);
	if(cmd == NULL){
		do_log(WARN, "Command not found");
		Message_answer_static(msg, "Command not found\n");
		return -1;
	}
	do_log(DEBUG, "Interpreting ascii message");
	int ret=cmd->callback(msg);
	if(ret==0){
		Message_answer_static(msg, "OK\n");
	}
	return 0;
}
