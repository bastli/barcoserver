#!/bin/bash

if [ $1 == "--help" ]; then

echo "To build the musl ARM gcc use --build-toolchain"
exit 0

fi

TOOLCHAIN=arm-linux-musleabihf
CC=${TOOLCHAIN}-gcc
if [ $1 == "--build-toolchain" ]; then
	pushd ..
	echo "Building arm-linux-musleabihf"
	git clone https://github.com/richfelker/musl-cross-make.git --depth 5
	cd musl-cross-make
	git pull
	make TARGET=arm-linux-musleabihf install -j8

	popd
	CC=../musl-cross-make/output/bin/arm-linux-musleabihf-gcc
fi

export CC

command -v ${CC} >/dev/null 2>&1 || { echo >&2 "I expect the compiler to be ${CC}, but I could not execute it. Please adapt CC and TOOLCHAIN in script."; exit 1; }


pushd ..

echo "Building libuv"

git clone https://github.com/libuv/libuv.git --depth 5
cd libuv
git pull
./autogen.sh
./configure CC=${CC} --host=${TOOLCHAIN} --disable-shared
make -j8

popd

./build.sh
