
TARGET=BarcoServer
DEPS=$(wildcard src/*.c)
LIBS=uv pthread dl
CFLAGS ?= -g -Wall
_CFLAGS=$(CFLAGS) -std=gnu99 -I./include/
CC ?= gcc

$(TARGET): $(patsubst %.c,%.o,$(DEPS))
	$(CC) $(_CFLAGS) $^ $(addprefix -l,$(LIBS)) -o $@
%.o:%.c
	$(CC) $(_CFLAGS) -c $^ -o $@

.PHONY: clean distclean
clean:
	rm -f $(patsubst %.c,%.o,$(DEPS))
distclean: clean
	rm -f $(TARGET)
